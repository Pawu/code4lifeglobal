﻿using System;
using System.Linq;
using ApiAiSDK.Model;
using PleggerService.Web.Hook;
using System.Collections.Generic;
using PleggerService.DialogFlow.Intent;

namespace PleggerService.DataService
{
    public class DataService : IDataService
    {
        private List<DialogFlowIntent> dialogFlowIntents = new List<DialogFlowIntent>() { new StudyCountIntent() };

        public AIResponse ProcessChatRequest(AIRequest request)
        {
            throw new System.NotImplementedException();
        }

        public DialogFlowWebhookResponse ProcessWebhookRequest(DialogFlowWebhookRequest request)
        {
            DialogFlowWebhookResponse response = null;
            foreach(DialogFlowIntent intent in dialogFlowIntents)
            {
                if (intent.handles(request.Result.Metadata.IntentName))
                {
                    return intent.process(request.Result);
                }
            }
            return unknownIntent();
        }

        private DialogFlowWebhookResponse unknownIntent()
        {
            DialogFlowWebhookResponse unknownResponse = new DialogFlowWebhookResponse();
            unknownResponse.DisplayText = "fränzu mage richtig done eieiei;";
            return unknownResponse;
        }
    }
}