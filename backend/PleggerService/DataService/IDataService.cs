﻿using ApiAiSDK.Model;
using PleggerService.Web.Hook;

namespace PleggerService.DataService
{
    public interface IDataService
    {
        AIResponse ProcessChatRequest(AIRequest request);
        DialogFlowWebhookResponse ProcessWebhookRequest(DialogFlowWebhookRequest request);
    }
}