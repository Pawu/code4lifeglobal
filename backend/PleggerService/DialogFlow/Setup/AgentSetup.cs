﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using PleggerService.Database;
using ApiAi;
using ApiAi.Models;

namespace PleggerService.DialogFlow.Setup
{
    public static class AgentSetup
    {
        private const int MaxEntityEntries = 30000; 
        private const string ConditionEntityName = "condition";
        private const string CountryEntityName = "country";
        
        public static void SetupAgent(IStudyDatabase db, string developerAccessToken)
        {
            var config = new ConfigModel {AccesTokenDeveloper = developerAccessToken};
            SetupEntity(db.GetAllConditions(), developerAccessToken, config, ConditionEntityName);
            SetupEntity(db.GetAllCountries(), developerAccessToken, config, CountryEntityName);
        }
        
        private static void SetupEntity(List<string> values, string developerAccessToken, ConfigModel config, string entityName)
        {
            var entities = EntityService.GetEntities(config);
            if (entities.Any(e => e.Name == entityName))
            {
                var entity = EntityService.GetEntities(config).First(e => e.Name == entityName);
                var entryList = EntityService.GetEntries(config, entity.Id);
                var entryStrings = entryList.Entries.Select(e => e.Value).ToArray();
                EntityService.DeleteEntries(config, entity.Id, entryStrings);   
            }
            var entries = new Dictionary<string, string[]>();
            values.ForEach(c => AddEntry(c, entries));
            EntityService.AddEntries(config, entityName, entries);
        }

        private static void AddEntry(string entryName, IDictionary<string, string[]> entries)
        {
            if (entryName != null && !entries.ContainsKey(entryName) && entries.Count < MaxEntityEntries)
            {
                var rgx = new Regex("[^a-zA-Z0-9 -]");
                entryName = rgx.Replace(entryName, "");
                entries.TryAdd(entryName, new string[0]);
            }
        }
    }
}