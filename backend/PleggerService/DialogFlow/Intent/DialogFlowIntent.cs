﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PleggerService.Web.Hook;

namespace PleggerService.DialogFlow.Intent
{
    public abstract class DialogFlowIntent
    {
        public abstract string IntentName { get; }

        public abstract DialogFlowWebhookResponse process(RequestResult result);

        public bool handles(string intentName)
        {
            return IntentName.Equals(intentName);
        }
    }
}
