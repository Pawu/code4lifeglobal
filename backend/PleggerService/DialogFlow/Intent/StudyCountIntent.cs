﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PleggerService.Web.Hook;
using PleggerService.Database;
using Microsoft.EntityFrameworkCore;

namespace PleggerService.DialogFlow.Intent
{
    public class StudyCountIntent: DialogFlowIntent
    {
        public override string IntentName { get { return "AmountOfStudies"; } }

        public override DialogFlowWebhookResponse process(RequestResult result)
        {
            if (result.Parameters.Condition.Length < 1 
                && result.Parameters.Country.Length < 1 
                && result.Parameters.Status.Length < 1
                && result.Parameters.Phase.Length > 0)
            {
                return PhaseOnly(result);
            }
            if (result.Parameters.Condition.Length < 1
                && result.Parameters.Country.Length < 1
                && result.Parameters.Status.Length > 0
                && result.Parameters.Phase.Length < 1)
            {
                return StatusOnly(result);
            }
            if (result.Parameters.Condition.Length < 1
                && result.Parameters.Country.Length > 0
                && result.Parameters.Status.Length < 1
                && result.Parameters.Phase.Length < 1)
            {
                return CountryOnly(result);
            }
            if (result.Parameters.Condition.Length > 0
                && result.Parameters.Country.Length < 1
                && result.Parameters.Status.Length < 1
                && result.Parameters.Phase.Length < 1)
            {
                return ConditionOnly(result);
            }
            return defaultFilter(result);
        }

        private DialogFlowWebhookResponse defaultFilter(RequestResult result)
        {
            DialogFlowWebhookResponse response = new DialogFlowWebhookResponse();
            response.Data = new ResponseData();
            response.Data.Results = new List<string>();
            List<Country> validCountries;
            using (var context = new StudyDatabaseContext())
            {
                var studies = context.Studies.Where(c => 
                    (result.Parameters.Phase.Length < 1 || result.Parameters.Phase.Any(x => x.Equals(c.phase)))).ToList();
                foreach (var study in studies)
                {
                    if (result.Parameters.Status.Length > 0)
                    {
                        context.Entry(study)
                            .Reference(c => c.OverallStatus)
                            .Load();
                        
                        var newStudies = studies.Where(s => s.OverallStatus != null && result.Parameters.Status.Any(x => x.Equals(s.OverallStatus.Status)));
                        if (!newStudies.Any())
                        {
                            continue;
                        } 
                    }
                    
                    if (result.Parameters.Condition.Length > 0)
                    {
                        context.Entry(study)
                            .Collection(c => c.Conditions)
                            .Load();

                        foreach (var condition in study.Conditions)
                        {
                            context.Entry(condition)
                                .Reference(c => c.Condition)
                                .Load();
                        }
                        
                        var newStudies = studies.Where(s => result.Parameters.Condition.Any(x => s.Conditions.Any(c => c.Condition.Name.Equals(x))));
                        if (!newStudies.Any())
                        {
                            continue;
                        } 
                    }
                    
                    if (result.Parameters.Country.Length > 0)
                    {
                        context.Entry(study)
                            .Collection(c => c.Countries)
                            .Load();

                        foreach (var condition in study.Countries)
                        {
                            context.Entry(condition)
                                .Reference(c => c.Country)
                                .Load();
                        }
                        
                        var newStudies = studies.Where(s => result.Parameters.Country.Any(x => s.Countries != null && s.Countries.Any(c => c.Country != null && c.Country.Name.Equals(x))));
                        if (!newStudies.Any())
                        {
                            continue;
                        } 
                    }
                    
                    context.Entry(study)
                        .Reference(r => r.Address)
                        .Load();
                    if (study != null && study.Address != null &&
                        study.Address.city != null)
                    {
                        response.Data.Results.Add(study.Address.city);
                    }
                }
            }
            var responseText = "I have found " + response.Data.Results.Count + " studies matching this description";
            response.DisplayText = responseText;
            response.Speech = responseText;
            return response;
        }

        private DialogFlowWebhookResponse PhaseOnly(RequestResult result)
        {
            DialogFlowWebhookResponse response = new DialogFlowWebhookResponse();
            response.Data = new ResponseData();
            response.Data.Results = new List<string>();
            List<Country> validCountries;
            using (var context = new StudyDatabaseContext())
            {
                var studies = context.Studies.Where(c => result.Parameters.Phase.Any(x => x.Equals(c.phase)))
                    .ToList();
                foreach (var study in studies)
                {
                    context.Entry(study)
                        .Reference(r => r.Address)
                        .Load();
                    if (study != null && study.Address != null &&
                        study.Address.city != null)
                    {
                        response.Data.Results.Add(study.Address.city);
                    }
                }

            }
            var responseText = "I have found " + response.Data.Results.Count + " studies with phase " + string.Join(", ", result.Parameters.Phase.ToArray());
            response.DisplayText = responseText;
            response.Speech = responseText;
            return response;
        }
        
        private DialogFlowWebhookResponse StatusOnly(RequestResult result)
        {
            DialogFlowWebhookResponse response = new DialogFlowWebhookResponse();
            response.Data = new ResponseData();
            response.Data.Results = new List<string>();
            List<Country> validCountries;
            using (var context = new StudyDatabaseContext())
            {
                var studies = context.Studies.Where(c => result.Parameters.Status.Any(x => x.Equals(c.OverallStatus)))
                    .ToList();
                foreach (var study in studies)
                {
                    context.Entry(study)
                        .Reference(r => r.Address)
                        .Load();
                    if (study != null && study.Address != null &&
                        study.Address.city != null)
                    {
                        response.Data.Results.Add(study.Address.city);
                    }
                }

            }
            var responseText = "I have found " + response.Data.Results.Count + " studies with status " + string.Join(", ", result.Parameters.Status.ToArray());
            response.DisplayText = responseText;
            response.Speech = responseText;
            return response;
        }

        private DialogFlowWebhookResponse CountryOnly(RequestResult result)
        {
            DialogFlowWebhookResponse response = new DialogFlowWebhookResponse();
            response.Data = new ResponseData();
            response.Data.Results = new List<string>();
            List<Country> validCountries;
            using (var context = new StudyDatabaseContext())
            {
                var countries = context.Countries.Where(c => result.Parameters.Country.Any(x => x.Equals(c.Name)))
                    .ToList();
                foreach (var country in countries)
                {
                    context.Entry(country)
                        .Collection(c => c.Studies)
                        .Load();

                    foreach (var conditionStudy in country.Studies)
                    {
                        context.Entry(conditionStudy)
                            .Reference(r => r.Study)
                            .Load();

                        context.Entry(conditionStudy.Study)
                            .Reference(r => r.Address)
                            .Load();
                        if (conditionStudy.Study != null && conditionStudy.Study.Address != null &&
                            conditionStudy.Study.Address.city != null)
                        {
                            response.Data.Results.Add(conditionStudy.Study.Address.city);
                        }
                    }
                }

            }
            var responseText = "I have found " + response.Data.Results.Count + " studies in " + string.Join(", ", result.Parameters.Country.ToArray());
            response.DisplayText = responseText;
            response.Speech = responseText;
            return response;
        }
        
        private DialogFlowWebhookResponse ConditionOnly(RequestResult result)
        {
            DialogFlowWebhookResponse response = new DialogFlowWebhookResponse();
            response.Data = new ResponseData();
            response.Data.Results = new List<string>();
            List<Country> validCountries;
            using (var context = new StudyDatabaseContext())
            {
                var condtions = context.Conditions.Where(c => result.Parameters.Condition.Any(x => x.Equals(c.Name)))
                    .ToList();
                foreach (var condition in condtions)
                {
                    context.Entry(condition)
                        .Collection(c => c.Studies)
                        .Load();

                    foreach (var conditionStudy in condition.Studies)
                    {
                        context.Entry(conditionStudy)
                            .Reference(r => r.Study)
                            .Load();

                        context.Entry(conditionStudy.Study)
                            .Reference(r => r.Address)
                            .Load();
                        if (conditionStudy.Study != null && conditionStudy.Study.Address != null &&
                            conditionStudy.Study.Address.city != null)
                        {
                            response.Data.Results.Add(conditionStudy.Study.Address.city);
                        }
                    }
                }

            }
            var responseText = "I have found " + response.Data.Results.Count + " studies regarding " + string.Join(", ", result.Parameters.Condition.ToArray());
            response.DisplayText = responseText;
            response.Speech = responseText;
            return response;
        }
        
        private DialogFlowWebhookResponse CountryCondition(RequestResult result)
        {
            DialogFlowWebhookResponse response = new DialogFlowWebhookResponse();
            response.Data = new ResponseData();
            response.Data.Results = new List<string>();
            List<Country> validCountries;
            using (var context = new StudyDatabaseContext())
            {
                var condtions = context.Conditions.Where(c => result.Parameters.Condition.Any(x => x.Equals(c.Name))).ToList();
                foreach (var condition in condtions)
                {
                    context.Entry(condition)
                        .Collection(c => c.Studies)
                        .Load();

                    foreach (var conditionStudy in condition.Studies)
                    {
                        context.Entry(conditionStudy)
                            .Reference(r => r.Study)
                            .Load();
                        
                        context.Entry(conditionStudy.Study)
                            .Reference(r => r.Address)
                            .Load();
                        if (conditionStudy.Study != null && conditionStudy.Study.Address != null 
                            && conditionStudy.Study.Address.city != null 
                            && conditionStudy.Study.Address.country != null 
                            && result.Parameters.Country.Any(x => conditionStudy.Study.Address.country.Equals(x)));
                        {
                            response.Data.Results.Add(conditionStudy.Study.Address.city);   
                        }
                    }
                }
                
            }
            var responseText = "I have found " + response.Data.Results.Count + " studies in" + result.Parameters.Country + " matching this description";
            response.DisplayText = responseText;
            response.Speech = responseText;
            return response;
        }
    }
}
