﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace PleggerService.Web.Hook
{
    public class DialogFlowWebhookResponse
    {
        [JsonProperty("contextOut")]
        public object[] ContextOut { get; set; }

        [JsonProperty("data")]
        public ResponseData Data { get; set; }

        [JsonProperty("displayText")]
        public string DisplayText { get; set; }

        [JsonProperty("source")]
        public string Source { get; set; }

        [JsonProperty("speech")]
        public string Speech { get; set; }
        
        public static DialogFlowWebhookResponse FromJson(string json) => JsonConvert.DeserializeObject<DialogFlowWebhookResponse>(json, ResponseConverter.Settings);
    }

    public class ResponseData
    {
        public List<string> Results { get; set; }
    }

    public static class ResponseSerialize
    {
        public static string ToJson(this DialogFlowWebhookResponse self) => JsonConvert.SerializeObject(self, ResponseConverter.Settings);
    }

    public static class ResponseConverter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
        };
    }
}