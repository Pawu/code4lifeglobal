﻿using Newtonsoft.Json;

namespace PleggerService.Web.Hook
{
    public class DialogFlowWebhookRequest
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("lang")]
        public string Lang { get; set; }

        [JsonProperty("originalRequest")]
        public OriginalRequest OriginalRequest { get; set; }

        [JsonProperty("result")]
        public RequestResult Result { get; set; }

        [JsonProperty("sessionId")]
        public string SessionId { get; set; }

        [JsonProperty("status")]
        public RequestStatus Status { get; set; }

        [JsonProperty("timestamp")]
        public string Timestamp { get; set; }
        
        public static DialogFlowWebhookRequest FromJson(string json) => JsonConvert.DeserializeObject<DialogFlowWebhookRequest>(json, RequestConverter.Settings);
    }

    public class RequestStatus
    {
        [JsonProperty("code")]
        public long Code { get; set; }

        [JsonProperty("errorType")]
        public string ErrorType { get; set; }
    }

    public class RequestResult
    {
        [JsonProperty("action")]
        public string Action { get; set; }

        [JsonProperty("actionIncomplete")]
        public bool ActionIncomplete { get; set; }

        [JsonProperty("contexts")]
        public object[] Contexts { get; set; }

        [JsonProperty("fulfillment")]
        public RequestFulfillment Fulfillment { get; set; }

        [JsonProperty("metadata")]
        public RequestMetadata Metadata { get; set; }

        [JsonProperty("parameters")]
        public RequestParameters Parameters { get; set; }

        [JsonProperty("resolvedQuery")]
        public string ResolvedQuery { get; set; }

        [JsonProperty("score")]
        public long Score { get; set; }

        [JsonProperty("source")]
        public string Source { get; set; }

        [JsonProperty("speech")]
        public string Speech { get; set; }
    }

    public class RequestParameters
    {
        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("country")]
        public string[] Country { get; set; }

        [JsonProperty("phase")]
        public string[] Phase { get; set; }

        [JsonProperty("condition")]
        public string[] Condition { get; set; }
        
        [JsonProperty("overall_status")]
        public string[] Status { get; set; }
    }

    public class RequestMetadata
    {
        [JsonProperty("intentId")]
        public string IntentId { get; set; }

        [JsonProperty("intentName")]
        public string IntentName { get; set; }

        [JsonProperty("webhookForSlotFillingUsed")]
        public string WebhookForSlotFillingUsed { get; set; }

        [JsonProperty("webhookUsed")]
        public string WebhookUsed { get; set; }
    }

    public class RequestFulfillment
    {
        [JsonProperty("messages")]
        public RequestMessage[] Messages { get; set; }

        [JsonProperty("speech")]
        public string Speech { get; set; }
    }

    public class RequestMessage
    {
        [JsonProperty("speech")]
        public string Speech { get; set; }

        [JsonProperty("type")]
        public long Type { get; set; }
    }

    public class OriginalRequest
    {
        [JsonProperty("data")]
        public RequestData Data { get; set; }

        [JsonProperty("source")]
        public string Source { get; set; }
    }

    public class RequestData
    {
        [JsonProperty("conversation")]
        public RequestConversation Conversation { get; set; }

        [JsonProperty("inputs")]
        public Input[] Inputs { get; set; }

        [JsonProperty("user")]
        public RequestUser User { get; set; }
    }

    public class RequestUser
    {
        [JsonProperty("user_id")]
        public string UserId { get; set; }
    }

    public class Input
    {
        [JsonProperty("arguments")]
        public RequestArgument[] Arguments { get; set; }

        [JsonProperty("intent")]
        public string Intent { get; set; }

        [JsonProperty("raw_inputs")]
        public RequestRawInput[] RawInputs { get; set; }
    }

    public class RequestRawInput
    {
        [JsonProperty("input_type")]
        public long InputType { get; set; }

        [JsonProperty("query")]
        public string Query { get; set; }
    }

    public class RequestArgument
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("raw_text")]
        public string RawText { get; set; }

        [JsonProperty("text_value")]
        public string TextValue { get; set; }
    }

    public class RequestConversation
    {
        [JsonProperty("conversation_id")]
        public string ConversationId { get; set; }

        [JsonProperty("conversation_token")]
        public string ConversationToken { get; set; }

        [JsonProperty("type")]
        public long Type { get; set; }
    }


    public static class RequestSerialize
    {
        public static string ToJson(this DialogFlowWebhookRequest self) => JsonConvert.SerializeObject(self, RequestConverter.Settings);
    }

    public static class RequestConverter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
        };
    }
}