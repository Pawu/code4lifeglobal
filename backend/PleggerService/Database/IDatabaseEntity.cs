﻿namespace PleggerService.Database
{
    public interface IDatabaseEntity
    {
        int Id { get; set; }
    }
}