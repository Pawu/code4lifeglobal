﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using PleggerService.Web;
using System.Linq;
using Microsoft.EntityFrameworkCore.Extensions.Internal;
using PleggerService.DataParsing;
using DbContext = Microsoft.EntityFrameworkCore.DbContext;

namespace PleggerService.Database
{
    public class StudyDatabase : IStudyDatabase
    {
        public StudyDatabase(){}

        public void InsertStudies(IEnumerable<ClinicalStudy> studies)
        {
            using (var context = new StudyDatabaseContext())
            {
                var clinicalStudies = studies as ClinicalStudy[] ?? studies.ToArray();
                foreach (var clinicalStudy in clinicalStudies)
                {
                    InsertStudy(clinicalStudy);
                }
            }
        }

        public void InsertStudy(ClinicalStudy study)
        {
            using (var context = new StudyDatabaseContext())
            {                 
                var status = study.OverallStatus;
                InsertOrUpdateStatus(ref status, context);
                study.OverallStatus = status;
                
                var conditions = study.Conditions ?? new List<StudyCondition>(); 
                foreach(var clinicalStudyCondition in conditions)
                {
                    InsertOrUpdateCondition(clinicalStudyCondition, context);
                }
                study.Conditions = conditions;
                
                var countries = study.Countries ?? new List<StudyCountry>(); 
                foreach (var clinicalStudyCountry in countries)
                {
                    InsertOrUpdateCountry(clinicalStudyCountry, context);
                }
                study.Countries = countries;
                
                context.Studies.Add(study);
                context.SaveChanges();
            }        
        }

        public void InsertOrUpdateStatus(ref OverallStatus overallstatus, StudyDatabaseContext context)
        {
            OverallStatus status = overallstatus;
            if (context.OverallStati.Any(s => s.Status == status.Status))
            {
                status = context.OverallStati.First(s => s.Status == status.Status);
            } else
            {
                context.OverallStati.Add(status);
                context.SaveChanges();
            }
            overallstatus = status;
        }
        
        public void InsertOrUpdateCondition(StudyCondition clinicalStudyCondition, StudyDatabaseContext context)
        {
            StudyCondition condition = clinicalStudyCondition;
            if (context.Conditions.Any(c => c.Name == condition.Condition.Name))
            {
                condition.Condition = context.Conditions.First(c => c.Name == condition.Condition.Name);
            } else
            {
                context.Conditions.Add(condition.Condition);
                context.SaveChanges();
            }
            clinicalStudyCondition = condition;
        }
        
        public void InsertOrUpdateCountry(StudyCountry clinicalStudyCountry, StudyDatabaseContext context)
        {
            StudyCountry country = clinicalStudyCountry;
            if (context.Countries.Any(c => c.Name == country.Country.Name))
            {
                country.Country =
                    context.Countries.First(c => c.Name == country.Country.Name);
            }
            else
            {
                context.Countries.Add(country.Country);
                context.SaveChanges();
            }
            clinicalStudyCountry = country;
        }

        public List<ClinicalStudy> GetStudies(string propertyName, object value)
        {
            using (var context = new StudyDatabaseContext())
            {
                var studyType = typeof(ClinicalStudy);
                var pInfo = studyType.GetProperty(propertyName);
                var studies = context.Studies.Where(s => pInfo.GetValue(s) == value);
                return studies.ToList();
            }
        }

        public List<string> GetAllConditions()
        {
            using (var context = new StudyDatabaseContext())
            {
                return context.Conditions.Select(c => c.Name).ToList();
            }
        }
        
        public List<string> GetAllCountries()
        {
            using (var context = new StudyDatabaseContext())
            {
                return context.Countries.Select(c => c.Name).ToList();
            }
        }

        public ClinicalStudy GetStudyByOrgId(string id)
        {
            using (var context = new StudyDatabaseContext())
            {
                var study = context.Studies.First(s => s.id_info.org_study_id == id);
                return study;
            }
        }

        public void ClearDatabase()
        {
            using (var context = new StudyDatabaseContext())
            {
                context.Studies.RemoveRange(context.Studies);
                context.SaveChanges();
            }
        }
        
        public dynamic GetAllStudies(int limit)
        {
            using (var context = new StudyDatabaseContext())
            {
                var studies = context.Studies.Take(limit);
                foreach (var clinicalStudy in studies)
                {
                    LoadStudyRelations(context, clinicalStudy);
                }
                return studies.ToList();
            }
        }

        public dynamic GetAllLocations()
        {
            using (var context = new StudyDatabaseContext())
            {
                return context.Addresses
                    .ToList();
            }
        }

        public dynamic GetStudy(int id)
        {
            using (var context = new StudyDatabaseContext())
            {
                var study = context.Studies.First(s => s.Id == id);
                LoadStudyRelations(context, study);
                return study;
            }
        }

        private static void LoadStudyRelations(DbContext context, ClinicalStudy study)
        {
            context.Entry(study)
                .Collection(b => b.Conditions)
                .Load();
            
            context.Entry(study)
                .Collection(b => b.Countries)
                .Load();
                    
            context.Entry(study)
                .Reference(b => b.Address)
                .Load();      
     
            context.Entry(study)
                .Reference(b => b.brief_summary)
                .Load();
            
            context.Entry(study)
                .Reference(b => b.id_info)
                .Load();
            
            context.Entry(study)
                .Reference(b => b.enrollment)
                .Load();
                   
            context.Entry(study)
                .Reference(b => b.OverallStatus)
                .Load();
            
            context.Entry(study)
                .Reference(b => b.completion_date)
                .Load();

            if (study.Conditions != null)
            {
                foreach (var condition in study.Conditions)
                {
                    context.Entry(condition)
                        .Reference(b => b.Condition)
                        .Load();        
                } 
            }

            if (study.Countries != null)
            {
                foreach (var country in study.Countries)
                {
                    context.Entry(country)
                        .Reference(b => b.Country)
                        .Load();        
                } 
            }
        }
    }
}