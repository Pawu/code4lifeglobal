﻿using System.Configuration;
using Microsoft.EntityFrameworkCore;

namespace PleggerService.Database
{
    public class StudyDatabaseContext : DbContext
    {
        public virtual DbSet<ClinicalStudy> Studies { get; set; }
        public virtual DbSet<Condition> Conditions { get; set; }
        public virtual DbSet<Country> Countries { get; set; }
        public virtual DbSet<OverallStatus> OverallStati { get; set; }
        public virtual DbSet<address_struct> Addresses { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //var connectionString = ConfigurationManager.ConnectionStrings["StudyDatabase"].ConnectionString;
            optionsBuilder.UseNpgsql("server=localhost;user id=postgres;password=Code4LifeYay!;database=studies");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // PostgreSQL uses the public schema by default - not dbo.
            modelBuilder.HasDefaultSchema("public");
            
            modelBuilder.Entity<Condition>()
                .HasIndex(c => c.Name)
                .IsUnique();
            
            modelBuilder.Entity<Country>()
                .HasIndex(c => c.Name)
                .IsUnique();
            
            modelBuilder.Entity<OverallStatus>()
                .HasIndex(s => s.Status)
                .IsUnique();

            modelBuilder.Entity<StudyCondition>()
                .HasOne(s => s.Condition)
                .WithMany(c => c.Studies)
                .HasForeignKey(c => c.ConditionId);

            modelBuilder.Entity<StudyCountry>()
                .HasOne(s => s.Country)
                .WithMany(c => c.Studies)
                .HasForeignKey(c => c.CountryId);

            modelBuilder.Entity<StudyCondition>()
                .HasOne(s => s.Study)
                .WithMany(c => c.Conditions)
                .HasForeignKey(c => c.StudyId);

            modelBuilder.Entity<StudyCountry>()
                .HasOne(s => s.Study)
                .WithMany(c => c.Countries)
                .HasForeignKey(c => c.StudyId);
            
            base.OnModelCreating(modelBuilder);
        }
    }
}
