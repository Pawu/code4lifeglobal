﻿using System.Collections.Generic;
using PleggerService.DataParsing;

namespace PleggerService.Database
{
    public interface IStudyDatabase
    {
        void InsertStudies(IEnumerable<ClinicalStudy> studies);
        
        void InsertStudy(ClinicalStudy studies);

        List<ClinicalStudy> GetStudies(string propertyName, object value);
        
        List<string> GetAllConditions();

        List<string> GetAllCountries();

        void ClearDatabase();
        dynamic GetAllStudies(int limit);
        dynamic GetAllLocations();
        dynamic GetStudy(int id);
    }
}