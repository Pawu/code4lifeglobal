﻿using Newtonsoft.Json;

namespace PleggerService.Database
{
    public class StudyCountry : IDatabaseEntity
    {
        public StudyCountry()
        {
        }
        
        public StudyCountry(ClinicalStudy study, Country country)
        {
            Country = country;
            Study = study;
        }

        [JsonIgnore]
        public int CountryId { get; set; }
        
        [JsonIgnore]
        public int StudyId { get; set; }
        public Country Country { get; set; }
        
        [JsonIgnore]
        public ClinicalStudy Study { get; set; }
        [JsonIgnore]
        public int Id { get; set; }
    }
}