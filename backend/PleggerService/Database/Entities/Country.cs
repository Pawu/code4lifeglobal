﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Mvc.ViewFeatures.Internal;
using Newtonsoft.Json;

namespace PleggerService.Database
{
    public class Country : IDatabaseEntity
    {

        public Country()
        {
        }

        public Country(string name)
        {
            Name = name;
        }
        
        public string Name { get; set; }
        
        [JsonIgnore]
        public ICollection<StudyCountry> Studies { get; set; }
        
        public override bool Equals(object o)
        {
            if (o.GetType() != this.GetType()) return false;
            var other = (Country) o;
            return string.Equals(Name, other.Name);
        }

        protected bool Equals(Country other)
        {
            return string.Equals(Name, other.Name) && Equals(Studies, other.Studies);
        }

        [JsonIgnore]
        public int Id { get; set; }
    }
}