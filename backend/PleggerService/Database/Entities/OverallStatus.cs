﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PleggerService.Database
{
    public class OverallStatus : IDatabaseEntity
    {

        public OverallStatus()
        {
        }

        public OverallStatus(string status)
        {
            Status = status;
        }

        public string Status { get; set; }

        public int Id { get; set; }
    }
}