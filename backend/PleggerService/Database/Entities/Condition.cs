﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace PleggerService.Database
{
    public class Condition : IDatabaseEntity
    {
        public Condition(){}

        public Condition(string name)
        {
            Name = name;
        }
        
        public string Name { get; set; }
        
        [JsonIgnore]
        public ICollection<StudyCondition> Studies { get; set; }

        public override bool Equals(object o)
        {
            if (o.GetType() != this.GetType()) return false;
            var other = (Condition) o;
            return string.Equals(Name, other.Name);
        }

        [JsonIgnore]
        public int Id { get; set; }
    }
}