﻿using Newtonsoft.Json;

namespace PleggerService.Database
{
    public class StudyCondition : IDatabaseEntity
    {
        public StudyCondition()
        {
        }

        public StudyCondition(ClinicalStudy study, Condition condition)
        {
            Condition = condition;
            Study = study;
        }
        
        [JsonIgnore]
        public int ConditionId { get; set; }
        
        [JsonIgnore]
        public int StudyId { get; set; }
        public Condition Condition { get; set; }
        
        [JsonIgnore]
        public ClinicalStudy Study { get; set; }
        
        [JsonIgnore]
        public int Id { get; set; }
    }
}