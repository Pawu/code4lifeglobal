﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using System.Threading;
using PleggerService.Database;

namespace PleggerService.DataParsing
{
    public class XmlStudiesParser
    {
        private const int BoundedSize = 2000;
        public int Count;
        public int AllCount;
        private readonly string _xmlDir;
        private readonly IStudyDatabase Db;
        private readonly BlockingCollection<ClinicalStudy> deserializedQueue;
        private ManualResetEvent deserializeFinished;
        private ManualResetEvent dbwriteFinished;
        
        private static Action<string> _consoleCallback;

        public static void SetConsoleCallback(Action<string> callback)
        {
            _consoleCallback = callback;
        }

        public XmlStudiesParser(string dir, IStudyDatabase db)
        {
            deserializeFinished = new ManualResetEvent(false);
            dbwriteFinished = new ManualResetEvent(false);
            _xmlDir = dir;
            Db = db;
            Count = 0;
            AllCount = 0;
            deserializedQueue = new BlockingCollection<ClinicalStudy>(BoundedSize);
        }

        public void Parse()
        {
            var timer = new Timer(new TimerCallback(PrintProgress), null, 0, 10000);
            ThreadPool.QueueUserWorkItem(c => SaveStudiesToDatabase());
            DeserializeStudiesRecursive();
            deserializeFinished.WaitOne();
            dbwriteFinished.WaitOne();
            timer.Dispose();
        }
        
        public void DeserializeStudiesRecursive()
        {
            string[] files;
            if (_xmlDir.EndsWith(".xml"))
            {
                files = new string[1];
                files[0] = _xmlDir;
            }
            else
            {
               files  = Directory.GetFiles(_xmlDir, "*.xml", SearchOption.AllDirectories);
            }
            foreach (var file in files)
            {
                 DeserializeStudy(file);
            }
            deserializeFinished.Set();
        }

        private void DeserializeStudy(string file)
        {
            using (var stream = new StreamReader(file))
            {
                var serializer = new XmlSerializer(typeof(ClinicalStudy));
                var study = (ClinicalStudy)serializer.Deserialize(stream);
                deserializedQueue.Add(study);
            }
        }

        private void SaveStudiesToDatabase()
        {
            while (Count < AllCount)
            {
                Db.InsertStudy(deserializedQueue.Take());
                Count++;
            }
            dbwriteFinished.Set();
        }

        private void PrintProgress(object o)
        {
            _consoleCallback(Count + " of " + AllCount + " files processed");
        }

        public void Reset()
        {
            Count = 0;
        }

        public int CountFilesRecursive()
        {
            if (_xmlDir.EndsWith(".xml"))
            {
                AllCount = 1;
                return AllCount;
            }
            AllCount = Directory.GetFiles(_xmlDir, "*.xml", SearchOption.AllDirectories).Length;
            return AllCount;
        }
    }
}