﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ApiAiSDK.Http;
using ApiAiSDK.Model;
using PleggerService.Database;
using PleggerService.DataService;
using PleggerService.Web.Hook;
using PleggerService.DataService;

namespace PleggerService.Controllers
{
    public class ChatController : Controller
    {
        
        private IDataService DataService { get; }
        private IStudyDatabase Database { get; }
        
        public ChatController()
        {
            DataService = new DataService.DataService();
            Database = new StudyDatabase();
        }
        
        [Route("api/chat/chat")]
        [HttpPost]
        public AIResponse Post(AIRequest chatRequest)
        {
            return DataService.ProcessChatRequest(chatRequest);
        }
        
        [Route("api/chat/hook")]
        [HttpPost]
        public DialogFlowWebhookResponse PostWebHookRequest([FromBody]DialogFlowWebhookRequest hookRequest)
        {
            ConsoleOutput("Webhook request received");
            return DataService.ProcessWebhookRequest(hookRequest);
        }

        [Route("api/chat/getallstudies/{limit}")]
        [HttpGet]
        public List<ClinicalStudy> GetAllStudies(int limit)
        {
            var studies = Database.GetAllStudies(limit);
            return studies;
        }
        
        [Route("api/chat/getalllocations")]
        [HttpGet]
        public List<address_struct> GetAllLocations()
        {
            var addresses = Database.GetAllLocations();
            return addresses;
        }
        
        private static void ConsoleOutput(string content)
        {
            Console.WriteLine("[" + DateTime.Now.ToLongTimeString() + "] " + content);
        }
    }

    public class EasyJson
    {
        public string testV {get;set;}
    }
    
}