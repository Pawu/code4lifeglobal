﻿using System;
using System.Configuration;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.IdentityModel.Protocols;
using PleggerService.Database;
using PleggerService.DataParsing;
using PleggerService.DialogFlow.Setup;

namespace PleggerService
{
    public class Program
    {
        private const string SetupCommand = "setup";
        private const string ImportCommand = "import";
        
        public static void Main(string[] args)
        {
            if (args.Length > 0)
            {
                string argumentOutput = args.Aggregate("", (current, str) => current + (str + " "));
                ConsoleOutput("Invoked with arguments: " + argumentOutput);
                ProcessArguments(args);
            } 
            else
            {
                ConsoleOutput("No arguments, continuing");
            }
            ConsoleOutput("Starting REST api...");
            BuildWebHost(null).Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseUrls("http://*:5001")
                .UseStartup<Startup>()
                .Build();

        private static void ConsoleOutput(string content)
        {
            Console.WriteLine("[" + DateTime.Now.ToLongTimeString() + "] " + content);
        }

        private static void ProcessArguments(string[] args)
        {
            if (args.Contains(ImportCommand) && args.Contains(SetupCommand))
            {
                var xmldir = args[Array.IndexOf(args, ImportCommand) + 1];
                if (!Directory.Exists(xmldir))
                {
                    throw new DirectoryNotFoundException("Directory " + xmldir + " does not exist!");
                }
                SetupDatabase(xmldir);
                SetupDialogFlowAgent();
                return;
            }
            if(args.Contains(SetupCommand))
            {
                SetupDialogFlowAgent();
                return;
            }
            if(args.Contains(ImportCommand))
            {
                var xmldir = args[Array.IndexOf(args, ImportCommand) + 1];
                SetupDatabase(xmldir);
            }
        }

        private static void SetupDatabase(string directory)
        {

            ConsoleOutput("Trying to parse xml files in directory: " + directory + "- writing them to postgresql database...");
            var dbContext = new StudyDatabaseContext();
            dbContext.Database.EnsureDeleted();
            dbContext.Database.EnsureCreated();
            var db = new StudyDatabase();
            XmlStudiesParser.SetConsoleCallback(ConsoleOutput);
            var parser = new XmlStudiesParser(directory, db);
            ConsoleOutput("Found " + parser.CountFilesRecursive() + " files in directory");
            parser.Parse();
            ConsoleOutput("Successfully written " + parser.Count + " studies to database");
            parser.Reset();
        }

        private static void SetupDialogFlowAgent()
        {
            ConsoleOutput("Adding entities and entries to DialogFlow...");
            AgentSetup.SetupAgent(new StudyDatabase(), ConfigurationManager.AppSettings["developerAccessToken"]);
            ConsoleOutput("Adding entities done!");
        }
    }
}