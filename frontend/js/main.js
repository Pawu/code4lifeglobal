var currentSet = 0;

$(function () {
	var mapHandler = new MapHandler($("#map"));
	var chatHandler = new ChatHandler(mapHandler);
	mapHandler.createMarkers(positonData[currentSet], function (handler) {
		handler.createCluster();
	});

	/*-----------------------------------------------
		Init frameworkds
	-----------------------------------------------*/
	$('#datatable').DataTable();

	$('input').iCheck({
		checkboxClass: 'icheckbox_minimal',
		radioClass: 'iradio_minimal',
		increaseArea: '20%' // optional
	});

	/*-----------------------------------------------
		Action handlers
	-----------------------------------------------*/
	$(".toggle.left").click(function () {
		if ($(".toggle.right").hasClass("active")) {
			$(".sidebar.right").toggleClass("active");
			$(".toggle.right").toggleClass("active");
		}
		$(".sidebar.left").toggleClass("active");
		$(this).toggleClass("active");
	});

	$(".toggle.right").click(function () {
		if ($(".toggle.left").hasClass("active")) {
			$(".sidebar.left").toggleClass("active");
			$(".toggle.left").toggleClass("active");
		}
		$(".sidebar.right").toggleClass("active");
		$(this).toggleClass("active");
	});

	$(".country-select").change(function () {
		var location = $(".country-select option:selected").text();
		if(location == "All"){
			mapHandler.reset();
		} else {
			mapHandler.zoomInLocation(location);
		}

	});

	$('input[name=map-type]').on('ifChecked', function (event) {
		mapHandler.clear();
		var value = $(this).val();
		if (value === "heat-map") {
			mapHandler.createHeatMap(positonData[currentSet]);
		} else {
			mapHandler.createMarkers(positonData[currentSet], function (handler) {
				handler.createCluster();
			});
		}
	});

	$(".text-input").keypress(function (e) {
		if (e.which === 13) {
			e.preventDefault();
			chatHandler.setText($(this).val(), "request");
			chatHandler.send($(this).val());
			$(this).val("");
		}
	});

});