/**
 * Creats a mapHandler object.
 * @param mapCanvas The canvas to draw.
 */
function MapHandler(mapCanvas) {
	this.options = {
		center: new google.maps.LatLng(20, 0),
		zoom: 3,
		mapTypeControl: false,
		streetViewControl: false,
		draggable: true,
		scrollwheel: true,
		zoomControl: true,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	this.map = new google.maps.Map(mapCanvas[0], this.options);
	this.map.set('styles', [{
		"featureType": "administrative",
		"elementType": "labels.text.fill",
		"stylers": [{"color": "#444444"}]
	}, {
		"featureType": "administrative.province",
		"elementType": "geometry.fill",
		"stylers": [{"color": "#ff0000"}, {"visibility": "off"}]
	}, {
		"featureType": "administrative.locality",
		"elementType": "geometry.fill",
		"stylers": [{"hue": "#ff0000"}, {"visibility": "on"}]
	}, {
		"featureType": "administrative.locality",
		"elementType": "geometry.stroke",
		"stylers": [{"visibility": "on"}]
	}, {
		"featureType": "administrative.neighborhood",
		"elementType": "geometry.fill",
		"stylers": [{"visibility": "off"}, {"color": "#ff0000"}]
	}, {
		"featureType": "landscape",
		"elementType": "all",
		"stylers": [{"color": "#f2f2f2"}]
	}, {
		"featureType": "landscape.man_made",
		"elementType": "geometry.fill",
		"stylers": [{"color": "#ffffff"}, {"visibility": "off"}]
	}, {
		"featureType": "landscape.man_made",
		"elementType": "geometry.stroke",
		"stylers": [{"color": "#bfbfbf"}, {"invert_lightness": true}, {"weight": "8.51"}, {"gamma": "6.84"}, {"lightness": "0"}]
	}, {
		"featureType": "landscape.man_made",
		"elementType": "labels.text.fill",
		"stylers": [{"visibility": "on"}]
	}, {
		"featureType": "landscape.man_made",
		"elementType": "labels.text.stroke",
		"stylers": [{"visibility": "off"}]
	}, {"featureType": "poi", "elementType": "all", "stylers": [{"visibility": "off"}]}, {
		"featureType": "poi",
		"elementType": "geometry.fill",
		"stylers": [{"visibility": "on"}]
	}, {
		"featureType": "poi",
		"elementType": "geometry.stroke",
		"stylers": [{"visibility": "on"}]
	}, {
		"featureType": "poi",
		"elementType": "labels.text.fill",
		"stylers": [{"visibility": "on"}]
	}, {
		"featureType": "poi",
		"elementType": "labels.text.stroke",
		"stylers": [{"visibility": "on"}]
	}, {
		"featureType": "poi.attraction",
		"elementType": "geometry.fill",
		"stylers": [{"visibility": "off"}, {"color": "#ff0000"}]
	}, {
		"featureType": "poi.business",
		"elementType": "geometry.fill",
		"stylers": [{"visibility": "off"}]
	}, {
		"featureType": "poi.government",
		"elementType": "geometry.fill",
		"stylers": [{"visibility": "on"}]
	}, {
		"featureType": "poi.medical",
		"elementType": "geometry.fill",
		"stylers": [{"visibility": "on"}]
	}, {
		"featureType": "poi.sports_complex",
		"elementType": "geometry.fill",
		"stylers": [{"visibility": "off"}]
	}, {
		"featureType": "road",
		"elementType": "all",
		"stylers": [{"saturation": -100}, {"lightness": 45}]
	}, {
		"featureType": "road.highway",
		"elementType": "all",
		"stylers": [{"visibility": "simplified"}]
	}, {
		"featureType": "road.arterial",
		"elementType": "labels.icon",
		"stylers": [{"visibility": "off"}]
	}, {"featureType": "transit", "elementType": "all", "stylers": [{"visibility": "off"}]}, {
		"featureType": "water",
		"elementType": "all",
		"stylers": [{"color": "#8e8e8e"}, {"visibility": "on"}]
	}, {
		"featureType": "water",
		"elementType": "geometry.fill",
		"stylers": [{"color": "#0580ff"}, {"weight": "0.37"}, {"gamma": "4.22"}]
	}]);
	this.geocoder = new google.maps.Geocoder();
	this.markers = [];
	this.cluster = null;
	this.heatmap = null;
}

/**
 * Zooms in a given address.
 * @param address The address to zoom in.
 * @param map The map to zoom in.
 */
MapHandler.prototype.zoomInLocation = function (address) {
	var _this = this;
	this.geocoder.geocode({'address': address}, function (results, status) {
		if (status === google.maps.GeocoderStatus.OK) {
			_this.map.setCenter(results[0].geometry.location);
			_this.map.fitBounds(results[0].geometry.viewport);
		}
	});
};

/**
 * Resets the Map to start.
 */
MapHandler.prototype.reset = function () {
	this.map.setCenter(new google.maps.LatLng(20, 0));
	this.map.setZoom(3);
};

/**
 * Creates a marker.
 * @param position Position object
 */
MapHandler.prototype.createMarker = function (object, position) {
	var marker = new google.maps.Marker({
		position: position,
		map: this.map,
		clickable: false,
		icon: 'upload/map/pin.png'
	});

	object.markers.push(marker);
};

/**
 * Creates markers.
 * @param positions Array with position objects.
 */
MapHandler.prototype.createMarkers = function (positions, _callback) {
	var _this = this;
	var itemsProcessed = 0;

	for (var i = 0; i < positions.length; i++) {
		_this.createMarker(_this, positions[i]);
	}
	_callback(_this);
};

/**
 * Creates the cluster for the map.
 */
MapHandler.prototype.createCluster = function () {
	var clusterOptions = {gridSize: 50, maxZoom: 17, imagePath: 'upload/map/m', zoomOnClick: false};
	this.cluster = new MarkerClusterer(this.map, this.markers, clusterOptions);
};

/**
 * Creats a heatmap.
 * @param positions The positions for the heatmap.
 */
MapHandler.prototype.createHeatMap = function (positions) {
	var heatmapData = [];
	for (var i = 0; i < positions.length; i++) {
		heatmapData.push(new google.maps.LatLng(positions[i].lat, positions[i].lng));
	}
	this.heatmap = new google.maps.visualization.HeatmapLayer({
		data: heatmapData
	});
	this.heatmap.setMap(this.map);
};

MapHandler.prototype.clear = function () {
	if (this.cluster !== null) {
		this.cluster.clearMarkers(null);
		this.cluster = null;
		this.markers.forEach(function (marker) {
			marker.setMap(null);
		});
		this.markers = [];
	}

	if (this.heatmap !== null) {
		this.heatmap.setMap(null);
		this.heatmap = null;
	}
};