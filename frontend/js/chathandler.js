/**
 * Creats a handler for the map.
 */
function ChatHandler(mapHandler) {
	this.accessToken = '4bbe12e6efbd42659adea39fbbf1f019';
	this.baseUrl = 'https://api.api.ai/v1/';
	this.session = new Date().getUTCMilliseconds();
	this.mapHandler = mapHandler;
}

/**
 * Sends a text to dialogflow.
 * @param text The text to send.
 */
ChatHandler.prototype.send = function (text) {
	var _this = this;
	$.ajax({
		type: "POST",
		url: _this.baseUrl + "query?v=20150910",
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		headers: {
			"Authorization": "Bearer " + _this.accessToken
		},
		data: JSON.stringify({query: text, lang: "en", sessionId: _this.session}),
		success: function (data) {
			if (data.result.fulfillment.speech === undefined || data.result.fulfillment.speech === "") {
				_this.setText(data.result.fulfillment.displayText, "answer");

				if (currentSet === 0) {
					currentSet = 1;
				} else if (currentSet === 1) {
					currentSet = 2;
				} else if (currentSet === 2) {
					currentSet = 0;
				}

				_this.mapHandler.clear();
				var value = $('input[name=map-type]:checked').val();
				if (value === "heat-map") {
					_this.mapHandler.createHeatMap(positonData[currentSet]);
				} else {
					_this.mapHandler.createMarkers(positonData[currentSet], function (handler) {
						handler.createCluster();
					});
				}
			} else {
				_this.setText(data.result.fulfillment.speech, "answer");
			}
		}
	});
};

/**
 * Sets the text in the chatbox.
 * @param text The text to set.
 * @param className The classname for speechbox.
 */
ChatHandler.prototype.setText = function (text, className) {
	var markup = "<div class='" + className + "'><span>" + text + "</span></div>"
	$(".chat").append(markup);
};